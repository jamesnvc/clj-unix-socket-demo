# servertest

Demo of serving HTTP over a Unix domain socket, instead of binding to a port.

It uses Aleph instead of http-kit (but should be mostly a drop-in replacement), but currently needs to override one tiny bit of Aleph to work.
It also can only do sockets on Linux, since macOS doesn't have Epoll.

Start like `(start-server! "path/to/socket.sock")` or by running the uberjar like `java -jar uberjar.jar foo.sock` and test with (recent) versions of curl like `curl --unix-socket foo.sock -X POST -d 'foo=bar&baz=quux' http:/path/thing`.
