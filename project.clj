(defproject servertest "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [io.netty/netty-transport-native-epoll "4.1.25.Final" :classifier "linux-x86_64"]
                 [aleph "0.4.6"]]

  :main servertest.core)
