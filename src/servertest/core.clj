(ns servertest.core
  (:gen-class)
  (:require
   [aleph.http :as http]
   [aleph.netty :as netty]
   [clojure.java.io :as io]
   [servertest.netty-patch :as patch])
  (:import
   (io.netty.channel.unix DomainSocketAddress)))

(defn handler
  [req]
  (prn "REQ" req)
  {:status 200
   :headers {"Content-Type" "text/plain"}
   :body "Hello!"})

(defn start-server! [socket-loc]
  (prn "epoll?" (netty/epoll-available?))
  (let [f (io/file socket-loc)]
    ;; need to
    (with-redefs [netty/start-server patch/start-server]
      (http/start-server handler {:socket-address (DomainSocketAddress. f)
                                  :epoll? true}))))

(defn -main [& args]
  (if-let [f (first args)]
    (start-server! f)
    (println "Supply a path")))
