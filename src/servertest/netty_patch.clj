(ns servertest.netty-patch
  (:require
   [aleph.netty :refer [epoll-available? pipeline-initializer AlephServer
                        wrap-future]]
   [manifold.deferred :as d])
  (:import
   (io.netty.bootstrap ServerBootstrap)
   (io.netty.channel ChannelOption ChannelPipeline EventLoopGroup)
   (io.netty.channel.epoll EpollEventLoopGroup EpollServerSocketChannel EpollServerDomainSocketChannel)
   (io.netty.channel.nio NioEventLoopGroup)
   (io.netty.channel.socket ServerSocketChannel)
   (io.netty.channel.socket.nio NioServerSocketChannel)
   (io.netty.channel.unix DomainSocketAddress)
   (io.netty.handler.ssl SslContext)
   (io.netty.util.concurrent DefaultThreadFactory)
   (java.net SocketAddress)))

(defn start-server
  [pipeline-builder
   ^SslContext ssl-context
   bootstrap-transform
   on-close
   ^SocketAddress socket-address
   epoll?]
  (let [num-cores      (.availableProcessors (Runtime/getRuntime))
        num-threads    (* 2 num-cores)
        thread-factory (DefaultThreadFactory. "aleph-netty-server-event-pool" false)
        closed?        (atom false)

        ^EventLoopGroup group
        (if (and epoll? (epoll-available?))
          (EpollEventLoopGroup. num-threads thread-factory)
          (NioEventLoopGroup. num-threads thread-factory))

        ^Class channel
        (if (and epoll? (epoll-available?))
          ;; [XXX] this is the bit that we need to add to make local sockets work
          (if (and socket-address (instance? DomainSocketAddress socket-address))
            EpollServerDomainSocketChannel
            EpollServerSocketChannel)
          NioServerSocketChannel)

        pipeline-builder
        (if ssl-context
          (fn [^ChannelPipeline p]
            (.addLast p "ssl-handler"
              (.newHandler ssl-context
                (-> p .channel .alloc)))
            (pipeline-builder p))
          pipeline-builder)]

    (try
      (let [b (doto (ServerBootstrap.)
                (.option ChannelOption/SO_BACKLOG (int 1024))
                (.option ChannelOption/SO_REUSEADDR true)
                (.option ChannelOption/MAX_MESSAGES_PER_READ Integer/MAX_VALUE)
                (.group group)
                (.channel channel)
                (.childHandler (pipeline-initializer pipeline-builder))
                (.childOption ChannelOption/SO_REUSEADDR true)
                (.childOption ChannelOption/MAX_MESSAGES_PER_READ Integer/MAX_VALUE)
                bootstrap-transform)

            ^ServerSocketChannel
            ch (-> b (.bind socket-address) .sync .channel)]
        (reify
          java.io.Closeable
          (close [_]
            (when (compare-and-set! closed? false true)
              (-> ch .close .sync)
              (-> group .shutdownGracefully)
              (when on-close
                (d/chain'
                 (wrap-future (.terminationFuture group))
                 (fn [_] (on-close))))))
          AlephServer
          (port [_]
            (-> ch .localAddress .getPort))
          (wait-for-close [_]
            (-> ch .closeFuture .await)
            (-> group .terminationFuture .await)
            nil)))

      (catch Exception e
        @(.shutdownGracefully group)
        (throw e)))))
